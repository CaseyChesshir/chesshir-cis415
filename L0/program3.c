// program3.c
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[])
{
    int i, *a;
    a = calloc(11, sizeof(int));
    // we were only allocating enough space for 
    // ten integers before, but now we have enough 
    // space in the array to hold all eleven values 

    for(i=0;i <= 10; i++)
    {
        a[i] = i;
    }
    for(i=0;i <= 10; i++)
    {
        printf("%d\n", a[i]);
    }
    free(a);
    // we need to free the block of memory that a 
    // was sitting inside 
    return 0;
}
