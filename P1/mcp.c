#define _POSIX_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>


void printstuff(){
    printf("\n\n\nI was called because of SIGUSR1, I am process %d\n\n\n",getpid());
}

void readysignal(){
    printf("process %d is ready to run\n",getpid());
}


int main(int argc, char *argv[]){
    signal(SIGUSR1,printstuff);
    signal(SIGUSR2,readysignal);
    if (argc < 2) {
        fputs("Usage: ./mcp programs.txt\n",stderr);
        return 1;
    }
    
    FILE *programs;
    
    programs = fopen(argv[1], "r");
    
    if (programs == NULL) {
        fputs("File isn't found.\n",stderr);
        return 1;
    }
    
    char cmd[60][60];
    char s[2] = " ";
    int lineNumber = 0;
    
    pid_t pid[1024];
    int p=0;
    
    
    while (fgets(cmd[lineNumber], 60, programs) != NULL) {
        char *temp = cmd[lineNumber];
        char *programName;
        char *programArgs[] = {};
        int numArgs = 0;
        programName = strtok(temp,s);
        programArgs[0] = programName;
        while (temp != NULL) {
            temp = strtok(NULL,s);
            programArgs[numArgs+1] = temp;
            numArgs++;
        }
        programArgs[numArgs] = NULL;
        
        for (int f=0; f <= numArgs; f++) {
            printf("programArgs[%d]: %s\n",f,programArgs[f]);
        }
        
        pid[p] = fork();
        if (pid[p]) {
            //execvp(programArgs[0],programArgs);
            // I am the parent.
            sleep(1);
            
            
            //printf("sending SIGUSR1 to %d\n",pid[p]);
            kill(pid[p], SIGUSR1);
            //printf("Just sent SIGUSR1 to %d\n",pid[p]);
            
            
            //sleep(1);
            //kill(pid[p], SIGUSR2);
            //sleep(1);
            //  kill(pid[p], SIGCONT);
        }else if (pid[p] == 0){
            signal(SIGUSR1,printstuff);
            signal(SIGUSR2,readysignal);
            // I am the child.
            execvp(programArgs[0],programArgs);/*cannot ever get this to run*/
            

            printf("I am process %d and I want to execute %s\n",getpid(),programArgs[0]);
            int sig = 0;
            sigset_t sigset;
            sigemptyset(&sigset);
            sigaddset(&sigset, SIGUSR1);
            sigaddset(&sigset, SIGUSR2);
            printf("About to wait for SIGUSR1\n");
            //sigwait(&sigset,sig);
            if (sig==SIGUSR2) {
            printf("congratulations\n");
            }
            //while (1) {}
            //printf("got it\n");
            printf("Just got SIGUSR1 from parent\n");

            //printf("about to exit\n\n");
            exit(0);
        }else{
            fputs("fork error\n",stderr);
            exit(1);
        }
        
        p++;
        
        lineNumber++;
    }
    

    fclose(programs);
    
    
    printf("\nFinished successfully\n");
    return 0;
}
