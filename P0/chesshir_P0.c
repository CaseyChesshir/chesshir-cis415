#include <stdio.h>

int main(){
    FILE *fp;
    while (1) {
        int c = getchar();
        if (c==EOF) {
            break;
        }
        if (c=='i') {
            int n = getchar();
            int buffer[n+1];
            int size = sizeof(int);
            int received = fread(buffer,size,n+1,stdin);
            if (received < n+1) {
                printf("Input error: not enough ints\n");
            }
            for (int i=0; i < n; i++) {
                printf("%d ",buffer[i]);
            }
        }
        
        if (c=='d') {
            int n = getchar();
            double buffer[n+1];
            int size = sizeof(double);
            int received = fread(buffer,size,n+1,stdin);
            if (received < n+1) {
                printf("Input error: not enough doubles\n");
            }
            for (int i=0; i < n; i++) {
                printf("%.10lg",buffer[i]);
            }
        }
        
        if (c=='s') {
            int n = getchar();

            char buffer[n+1];
            int size = sizeof(char);
            int received = fread(buffer,size,n+1,stdin);
            if (received < n+1) {
                printf("Input error: not enough chars\n");
            }
            for (int i=0; i < n; i++) {
                printf("%c",buffer[i]);
            }
        }
        
        if (c=='n') {
            printf("\n");
        }
    }
}
