Contents of directory:
	ATP.c
		Main ATP server code. 
	functions.c
		Function implementations. 
	functions.h
		Function declarations. 
	structs.h
		All custom structs used in ATP.c are defined here. 
	Readme.txt 
		This file. 



Main strategy: 
	Initialize new client, give them a thread when they connect, let them tell me what they're interested in. 


PART 1
	Mostly working, but "terminating" publishers and subscribers isn't working. 
PART 2
	Working except for pthread_joining/termination.  
PART 3
	Data structures and routines defined, but IPC not playing nicely. 
PART 4
	Not working. 
PART 5
	Not working. 
PART 6
	Each pub increments a counter by 3, each sub increments the counter by 5, each topic increments the counter by 7, (need relatively prime numbers) so by the end of the program, the counter should be n*3 + m*5 + t*7. 

	e.g. for n = 10, m = 10, t = 10, counter should be 150. This solution is unique and can only be achieved with this combination of pubs/subs/topics.

	Can't quite get the shared data structure (regular integer pointer) to play nicely between processes, otherwise, this would work.  