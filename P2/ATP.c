#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <pthread.h>
#include <time.h>
#include <math.h>

#include "functions.c"
#include "structs.h"

#define ENTRYSIZE 128
#define MAXENTRIES 128
#define IOBUFFERSIZE 100





int main(int argc, char *argv[]){
    if (argc != 4) {
        printf("Usage: ./ATP n m t\n");
		exit(1);
    }
    int n = atol(argv[1]); // n publisher processes
    int m = atol(argv[2]); // m subscriber processes
    int t = atol(argv[3]); // t topics

    pid_t publishers[n];
    pid_t subscribers[m];
	pthread_t pubthreads[n];
	pthread_t subthreads[m];
	struct Record records[n+m];
	struct TopicQueue *queues[t];
	for (int i=0; i < t; i++){
		queues[t] = queue(t);
	}	

	sem_t *empty_sem;
	sem_t *full_sem;

	pthread_mutex_t *lock;

    char *connect = "connect";
    char *topic = "topic";
    char *accept = "accept";
    char *reject = "reject";
    char buffer[1024];
    char line[64];

    int ATPpubpipe[2]; // pipe from ATP to publisher
    int pubATPpipe[2]; // pipe from publisher to ATP
    int ATPsubpipe[2]; // pipe from ATP to subscriber
    int subATPpipe[2]; // pipe from subscriber to ATP

	int *counter = 0;

	pid_t ATP = fork();
	if (ATP==-1){
		perror("fork error\n");
		exit(1);
	}
	if (ATP==0){
		for (int i=0; i < n; i++) {
			pipe(ATPpubpipe);
			pipe(pubATPpipe);
			publishers[i] = fork();
			if (publishers[i] == -1) {
				perror("fork error\n");
				exit(1);
			}
			if (publishers[i] == 0){
				// I am a child
				counter += 3;
				char *publisherConnect = malloc(sizeof(char)*64);
				sprintf(publisherConnect, "%s %d %s\n","pub",getpid(),connect);
				int *interested = malloc(sizeof(int)*t);
				decideTopics(interested,t);
				write(pubATPpipe[1],publisherConnect,strlen(publisherConnect)+1);
				for (int k=0; k < t; k++) {
					if (interested[k]){	
						char *publisherTopic = malloc(sizeof(char)*64);
						sprintf(publisherTopic, "%s %d %s %d\n","pub",getpid(), topic, k+1);
						read(ATPpubpipe[0],line,sizeof(char)*6); // get accept message
						if(strcmp(accept,line)==0 ) {
							write(pubATPpipe[1],publisherTopic, strlen(publisherTopic)+1);
						}
						if (strcmp(reject,line)==0) {
							perror("error from ATP server\n");
							exit(1);
						}
					}
				}
				write(pubATPpipe[1],"end",strlen("end"));
				//exit(0);
			}else{
				// I am the parent
				read(pubATPpipe[0],buffer, 18); // get connect message
				write(STDOUT_FILENO,buffer,18); // write out connect message

				struct Record *record= malloc(sizeof(struct Record));
				threadInit(record,ATPpubpipe,pubATPpipe,"pub");
				
				pthread_create(&pubthreads[i],NULL,publishTopics,record);		
				pthread_join(pubthreads[i],NULL);
				printf("publisher joined\n");
				records[i] = *record;
			}
		}
		for (int i=0; i < m; i++) {
			pipe(ATPsubpipe);
			pipe(subATPpipe);
			subscribers[i] = fork();
			if (subscribers[i] == -1) {
				perror("fork error\n");
				exit(1);
			}
			if (subscribers[i] == 0){
				// I am a child
				counter += 5;
				char *subscriberConnect = malloc(sizeof(char)*64);
				sprintf(subscriberConnect, "%s %d %s\n","sub",getpid(),connect);
				write(subATPpipe[1],subscriberConnect,strlen(subscriberConnect)+1);
				int *interested = malloc(sizeof(int)*t);
				decideTopics(interested,t);
				for (int k=0; k < t; k++) {
					if (interested[k]){
						char *subscriberTopic = malloc(sizeof(char)*64);
						sprintf(subscriberTopic, "%s %d %s %d\n","sub",getpid(), topic, k+1);
						read(ATPsubpipe[0],line,sizeof(char)*6); // get accept message
						if(strcmp(accept,line)==0 ) {
							write(subATPpipe[1],subscriberTopic, strlen(subscriberTopic)+1);
						}
						if (strcmp(reject,line)==0) {
							perror("error from ATP server\n");
							exit(1);
						}
					}
				}
				write(subATPpipe[1],"end",strlen("end"));
			}else{
				// I am the parent
				read(subATPpipe[0],buffer, 18); // get connect message
				write(STDOUT_FILENO,buffer,18); // write out connect message
				
				struct Record *record= malloc(sizeof(struct Record));
				threadInit(record,ATPsubpipe,subATPpipe,"sub");
				pthread_create(&subthreads[i],NULL,subscribeTopics,record);		
				pthread_join(subthreads[i],0);
				printf("subscriber joined\n");
				records[i+n] = *record;
			}
		}
		struct Archive *archive;
		pthread_t archivingThread;
		pthread_create(&archivingThread,NULL,archiveTopics,archive);
		pthread_join(archivingThread,0);
		errno = 0;
		while (wait(NULL) && ECHILD != errno)
		{
			// wait
		}

		printf("everything finished!\n");
		exit(0);
	}
}
