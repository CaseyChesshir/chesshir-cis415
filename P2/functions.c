#include "structs.h"
#include "functions.h"

void getBin(int num, char *str){
	*(str+5) = '\0';
	int mask = 0x10 << 1;
	while(mask >>= 1){
		*str++ = !!(mask & num) + '0';
	}
}

void decideTopics(int *array, int t){
    // array - which topics does this client want
    // t - number of elements in array
    /*
     Randomly decides which topics this publisher or
     subscriber is interested in publishing or subscribing
     to. This can easily be replaced by any other
     "decision" function.
     */
    srand(getpid());
    for (int n = 0; n < t; n++){
        if (rand() % 2){
            array[n] = 1;
        }else{
            array[n] = 0;
        }
    }
}

void *archiveTopics(void *arg){
	struct Archive *archive = (struct Archive*) arg;
	printf("Archiving!\n");
}

void threadInit(struct Record *Record, int ATPclientpipe[2], int clientATPpipe[2], char *type){
    /*
     Initializes a thread record with its appropriate pipes.
     */
    Record->ATPclientpipe[0] = ATPclientpipe[0];
    Record->ATPclientpipe[1] = ATPclientpipe[1];
    Record->clientATPpipe[0] = clientATPpipe[0];
    Record->clientATPpipe[1] = clientATPpipe[1];
	Record->type = type;
}

void *publishTopics(void *arg){
    /*
     The ATP server is given this function to compute with
     each new publisher. This handles the communication from
     the publisher to the server after the initial connection.
     */
    struct Record *record = (struct Record*) arg;
    record->type = "pub";
    char *received = malloc(sizeof(char)*64);
    int interested = 0;
    while (strncmp("end",received,3)!=0){
        write(record->ATPclientpipe[1],"accept",strlen("accept"));
        read(record->clientATPpipe[0],received,sizeof(char)*64);
        write(STDOUT_FILENO,received,sizeof(char)*strlen(received));
        char *temp = received;
        temp = strtok(received," "); // temp is "pub"
        temp = strtok(NULL, " "); // temp is the pubid
        temp = strtok(NULL, " "); // temp is "topic"
        temp = strtok(NULL, " "); // temp is the topic number
        temp = strtok(temp,"\n"); // removes pesky newline char
        if (temp != NULL){ // temp is NULL if this was the first message
            interested += 1 << atol(temp);
        }
    }
    record->selectedTopics = &interested;
	printf("selectedTopics: %d",record->selectedTopics);
    pthread_exit(0);
}

void *subscribeTopics(void *arg){
    struct Record *record = (struct Record*) arg;
    record->type = "sub";
    char *received = malloc(sizeof(char)*64);
    int interested = 0;
    while (strncmp("end",received,3)!=0){
        write(record->ATPclientpipe[1],"accept",strlen("accept"));
        read(record->clientATPpipe[0],received,sizeof(char)*64);
        write(STDOUT_FILENO,received,sizeof(char)*strlen(received));
        char *temp = received;
        temp = strtok(received," "); // temp is "pub"
        temp = strtok(NULL, " "); // temp is the pubid
        temp = strtok(NULL, " "); // temp is "topic"
        temp = strtok(NULL, " "); // temp is the topic number
        temp = strtok(temp,"\n"); // removes pesky newline char
        if (temp != NULL){ // temp is NULL if this was the first message
            interested += 1 << atol(temp);
        }
    }
    record->selectedTopics = &interested;
	printf("selectedTopics: %d",record->selectedTopics);
    pthread_exit(0);
}

void enqueue(struct Entry *entry, struct TopicQueue *queue){
	struct timespec timestamp;
    
    clock_gettime(CLOCK_MONOTONIC, &timestamp);

	sem_wait(queue->full);
	pthread_mutex_lock(&(queue->lock));
		//put the entry in 
	pthread_mutex_unlock(&(queue->lock));
	sem_post(queue->full);
	entry->timestamp = timestamp.tv_nsec;    
	//queue->array[(queue->&tail)] = entry;
    queue->tail += sizeof(struct Entry);
}

void dequeue(struct Entry *entry, struct TopicQueue *queue){
	// entry is the output	
	struct timespec timestamp;
	clock_gettime(CLOCK_MONOTONIC, &timestamp);
	if (timestamp.tv_nsec - entry->timestamp > 1000){
		// this is an old entry 
		// it's one second old 
		pthread_mutex_lock(&(queue->lock));
		queue->head += sizeof(struct Entry);
		entry = queue->head;
		pthread_mutex_unlock(&(queue->lock));
		sem_post(queue->empty);
	}
	sem_wait(queue->empty);
	pthread_mutex_lock(&(queue->lock));
    entry = queue->head;
    queue->head += sizeof(struct Entry);
	pthread_mutex_unlock(&(queue->lock));
	
}
