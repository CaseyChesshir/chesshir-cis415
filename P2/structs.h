#ifndef STRUCTS_H
#define STRUCTS_H


#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <pthread.h>
#include <time.h>
#include <math.h>

#define ENTRYSIZE 128
#define MAXENTRIES 128
#define IOBUFFERSIZE 100



struct Entry{
    unsigned long long timestamp;
    int pubID;
    int data[ENTRYSIZE];
};

struct TopicQueue{
	sem_t *full;
	sem_t *empty;
    int *head;
    int *tail;
    int size;
    struct Entry *array;
	pthread_mutex_t *lock;
};

struct TopicQueue *queue(int size){
    struct TopicQueue *queue = malloc(sizeof(struct TopicQueue));
    if (!queue)return NULL;
	sem_init(&(queue->full), 1, MAXENTRIES); // queue is not initially full 
	sem_init(&(queue->empty), 1, MAXENTRIES); // queue is initially empty 
    queue->size = size;
    queue->head = -1;
    queue->tail = -1;
    queue->array = malloc(queue->size*sizeof(struct Entry));
    if(!queue->array)return NULL;
    return queue;
};

struct Record{
    char *type; // either "pub" or "sub"
    int ATPclientpipe[2];
    int clientATPpipe[2];
    int *selectedTopics;
    char *topics;
    char *data;
};

#endif
