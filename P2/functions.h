#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <math.h>
#include <time.h>
#include <stdint.h>
void decideTopics(int *array, int t);
void threadInit(struct Record *Record, int ATPclientpipe[2], int clientATPpipe[2], char *type);
void *publishTopics(void *arg);
void *subscribeTopics(void *arg);
void getBin(int num, char *str);
int isEmpty(struct TopicQueue *queue);
int isFull(struct TopicQueue *queue);
void enqueue(struct Entry *entry, struct TopicQueue *queue);
void dequeue(struct Entry *entry, struct TopicQueue *queue);

#endif
